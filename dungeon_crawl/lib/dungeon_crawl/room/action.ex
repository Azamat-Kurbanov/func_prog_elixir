defmodule DungeonCrawl.Room.Action do
  alias DungeonCrawl.Room.Action
  defstruct id: nil,
            label: nil

  defimpl DungeonCrawl.Display do
    def info(action), do: action.label
  end

  defimpl String.Chars do
    def to_string(action), do: action.label
  end

  def forward do
    %Action{id: :forward, label: "Move forward."}
  end

  def rest do
    %Action{id: :rest, label: "Take a better look and rest."}
  end

  def search do
    %Action{id: :search, label: "Search the room."}
  end
end
