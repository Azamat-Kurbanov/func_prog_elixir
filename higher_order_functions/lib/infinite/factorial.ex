defmodule Infinite.Factorial do
  import Stream, only: [iterate: 2]
  def of(0), do: 1

  def of(n) when n > 0 do
    iterate(1, &(&1 + 1))
    |> Enum.take(n)
    |> Enum.reduce(&(&1 * &2))
  end
end
