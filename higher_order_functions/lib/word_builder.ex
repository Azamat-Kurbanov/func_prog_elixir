defmodule WordBuilder do
  def build(alphabet, positions) do
    positions
    |> Enum.map(&String.at(alphabet, &1))
    |> Enum.join
  end

  def build2(alphabet, positions) do
    partial = fn at -> String.at(alphabet, at) end
    letters = Enum.map positions, partial
    Enum.join letters
  end
end
