defmodule Fibonacci do
    def run(n) do
        {0, 1}
        |> Stream.unfold(fn {n1, n2} -> {n1, {n2, n1 + n2}} end)
        |> Enum.take(n)
    end
end