defmodule GeneralStore do
  def filter_items([], magic: _), do: []
  def filter_items([item = %{magic: magic}| tail], magic: magic) do
    [item | filter_items(tail, magic: magic)]
  end
  def filter_items([_head|tail], magic: magic), do: filter_items(tail, magic: magic)
end
