defmodule SumTail do
  def up_to(n) do
    up_to_helper(n, 0)
  end

  defp up_to_helper(0, acc), do: acc
  defp up_to_helper(n, acc) do
    up_to_helper(n - 1, n + acc)
  end
end
