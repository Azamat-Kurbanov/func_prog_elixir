defmodule MyList do
  def max([head|tail]) do
    h_max(head, tail)
  end

  defp  h_max(n, []), do: n
  defp h_max(n, [head|tail]) when n >= head do
    h_max(n, tail)
  end
  defp h_max(_n, [head|tail]) do
    h_max(head, tail)
  end

  def min([head|tail]), do: h_min(head, tail)

  defp h_min(n, []), do: n
  defp h_min(n, [head|tail]) when n <= head  do
    h_min(n, tail)
  end
  defp h_min(_n, [head|tail]) do
    h_min(head, tail)
  end
end
