defmodule MathTail do
  def sum(list), do: sum_helper(list, 0)

  defp sum_helper([], acc), do: acc
  defp sum_helper([head|tail], acc) do
    sum_helper(tail, acc + head)
  end
end
