defmodule TotalPoints do
    def total(%{strng: strength, dext: dexterity, intell: intelligence}) do
        strength * 2 + dexterity * 3 + intelligence * 3
    end
end