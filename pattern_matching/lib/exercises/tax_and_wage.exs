defmodule IncomeTax do
  def calculate_tax(income) do
    cond do
      income == 2000 -> 2000
      income <= 3000 -> tax_calculator(income, 5)
      income <= 6000 -> tax_calculator(income, 10)
      income -> tax_calculator(income, 15)
    end
  end

  defp tax_calculator(income, percent) do
    income * (percent / 100)
  end
end

res = case Integer.parse(IO.gets("Your salary\n")) do
  {salary, _} -> "Your income tax is #{IncomeTax.calculate_tax(salary)} and net wage #{trunc(salary - IncomeTax.calculate_tax(salary))}"
  {:error} -> "Please enter number"
end

IO.puts res

